const webpack = require('webpack')
const browserSync = require('browser-sync')
const historyApiFallback = require('connect-history-api-fallback')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const proxy = require('http-proxy-middleware')
const config = require('../webpack.config.js')

const bundler = webpack(config)

browserSync({
  port: 4000,
  ui: {
    port: 4001
  },
  server: {
    baseDir: 'src',

    middleware: [
      proxy('/api', { target: 'http://localhost:3000', changeOrigin: true }),
      historyApiFallback(),
      webpackDevMiddleware(bundler, {
        publicPath: config.output.publicPath,
        logLevel: 'warn',
        stats: {
          assets: false,
          colors: true,
          version: false,
          hash: false,
          timings: false,
          chunks: false,
          chunkModules: false
        }
      }),
      webpackHotMiddleware(bundler, {
        reload: true
      })
    ]
  },
  files: [
    'src/*.html'
  ]
})
