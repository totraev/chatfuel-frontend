const browserSync = require('browser-sync')
const proxy = require('http-proxy-middleware')

browserSync({
  port: 4000,
  ui: {
    port: 4001
  },
  server: {
    baseDir: 'dist',

    middleware: [
      proxy('/api', { target: 'http://localhost:3000', changeOrigin: true })
    ]
  },
  files: [
    'dist/*.html'
  ]
})
