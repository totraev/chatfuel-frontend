import { createStore, applyMiddleware, Store } from 'redux';
import { stateTransformer } from 'redux-seamless-immutable';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducers, { State } from './rootReducer';

const loggerMiddleware = createLogger({ stateTransformer, collapsed: false });

/**
 * Create store for production env
 */
function configureStoreProd(initialState: State): Store<State> {
  const middlewares = [thunk];

  const store = createStore(
    reducers,
    initialState,
    applyMiddleware(...middlewares),
  );

  return store;
}

/**
 * Create store for development env
 */
function configureStoreDev(initialState: State): Store<State> {
  const middlewares = [thunk, loggerMiddleware];

  const store = createStore(
    reducers,
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares)),
  );

  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      const nextReducer = require('./rootReducer').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}

const configureStore = process.env.NODE_ENV === 'production'
  ? configureStoreProd
  : configureStoreDev;


export default configureStore;
