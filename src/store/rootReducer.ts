import { combineReducers } from 'redux-seamless-immutable';

import appReducer from '../containers/App/duck';
import { ImmutableState as AppState } from '../containers/App/types';

/**
 * Global App state
 */
export type State = {
  app: AppState
};

/**
 * Root reducer
 */
export default combineReducers<State>({
  app: appReducer
});
