import React from 'react';
import { Props } from './types';
import './styles.css';


const Input: React.SFC<Props> = ({ loading, ...props }) => (
  <div styleName={loading ? 'loading' : 'input-wrap'}>
    <input styleName="input" type="text" {...props}/>
  </div>
);

Input.defaultProps = {
  value: '',
  onChange: () => {},
  loading: false
};

export default Input;
