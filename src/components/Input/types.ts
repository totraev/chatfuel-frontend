import { ChangeEvent } from 'react';

export type Props = {
  value: string
  loading: boolean
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
};
