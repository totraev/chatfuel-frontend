import React from 'react';
import { Props } from './types';
import './styles.css';

import UserItem from '../UserItem';
import Loader from '../Loader/';


class UserList extends React.PureComponent<Props> {
  static defaultProps: Props = {
    users: [],
    loader: false
  };

  render() {
    const { users, loader } = this.props;

    return (
      <div styleName="user-list">
        {
          users.length > 0
            ? users.map(user => <UserItem key={user.id} {...user}/>)
            : loader
              ? <Loader loading={true}/>
              : <p styleName="empty">There are no users</p>
        }
      </div>
    );
  }
}

export default UserList;
