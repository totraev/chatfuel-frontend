export type User = {
  id: string
  avatarUrl: string
  name: string
};

export type Props = {
  users: User[]
  loader: boolean
};
