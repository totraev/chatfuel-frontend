export type Props = {
  id: string
  avatarUrl: string
  name: string
};
