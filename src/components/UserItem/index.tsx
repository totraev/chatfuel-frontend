import React from 'react';
import { Props } from './types';
import './styles.css';


class UserItem extends React.PureComponent<Props> {
  static defaultProps: Props = {
    id: null,
    avatarUrl: '#',
    name: ''
  };

  render() {
    const { name, avatarUrl } = this.props;

    return (
      <div styleName="user-item">
        <img styleName="avatar" src={avatarUrl} alt={name}/>

        <div styleName="info">
          <span styleName="name">{name}</span>  
        </div>
      </div>
    );
  }
}

export default UserItem;
