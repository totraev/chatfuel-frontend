import React from 'react';
import './styles.css';
import { Props } from './types';

export const Loader: React.SFC<Props> = ({ loading }) => (
  <div styleName={loading ? 'active' : 'loader'}>
    <div styleName="circle-1"/>
    <div styleName="circle-2"/>
    <div styleName="circle-3"/>
  </div>
);

export default Loader;
