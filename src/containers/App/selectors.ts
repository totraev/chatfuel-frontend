import { createSelector, Selector } from 'reselect';
import { User } from './types';
import { State } from '../../store/rootReducer';

const getSearchTerm: Selector<State, string> = state => state.app.searchTerm;
const getUserArr: Selector<State, User[]> = state => state.app.users;
const getFilterBy: Selector<State, string[]> = state => state.app.filterBy;

export const filterSelector = createSelector<State, string, string[], User[], User[]>(
  getSearchTerm,
  getFilterBy,
  getUserArr,
  (searchTerm, filterBy, users) => {
    const regExp = new RegExp(`^${searchTerm}`, 'i');

    return users.filter(user => filterBy.some(field => regExp.test(user[field])));
  }
);
