import { ImmutableObject } from 'seamless-immutable';

/**
 * State
 */
export type User = {
  id: string
  name: string
  avatarUrl: string
};

export type FilterLoader = {
  requestId: string
  value: boolean
};

export type AppState = {
  searchTerm: string
  filterBy: string[]
  nextPageUrl: string
  filterLoader: FilterLoader
  usersLoader: boolean
  users: User[]
};

export type ImmutableState = ImmutableObject<AppState>;


/**
 * Api
 */
export type ResData = {
  result: User[]
  previousPageUrl?: string
  nextPageUrl?: string
};


/**
 * Component props
 */
export type StateProps = {
  searchTerm: string
  filterBy: string[]
  nextPageUrl: string
  filterLoader: boolean
  usersLoader: boolean
  users: User[]
};

export type DispatchProps = {
  updateSearch: (value: string) => void
  fetchUsers: (searchTerm:string) => void
  appendUsers: (nextPageUrl: string) => void
  setFilterLoader: (value: boolean) => void
  setUsersLoader: (value: boolean) => void
};

export type ComponentProps = {
  debounceTime?: number
};

export type Props = StateProps & DispatchProps & ComponentProps;
