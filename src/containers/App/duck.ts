import Immutable from 'seamless-immutable';
import { ThunkAction } from 'redux-thunk';
import nanoid from 'nanoid';
import { apiFetch } from '../../utils/api';
import { ImmutableState, AppState, User, ResData, FilterLoader } from './types';
import { State } from '../../store/rootReducer';
import createReducer, { Action } from '../../utils/createReducer';


/**
 * Constants
 */
export const INIT_STATE = 'app/INIT_STATE';
export const UPDATE_SEARCH = 'app/UPDATE_SEARCH_TERM';
export const ADD_USERS = 'app/ADD_USERS';
export const SET_USERS = 'app/SET_USERS';
export const SET_LOADER = 'app/SET_FILTER_LOADER';
export const SET_USER_LOADER = 'app/SET_USER_LOADER';
export const SET_NEXT_PAGE = 'app/SET_NEXT_PAGE';

/**
 * Actions creators
 */
export function initState(): Action<void> {
  return {
    type: INIT_STATE
  };
}

export function addUsers(users: User[] = []): Action<User[]> {
  return {
    type: ADD_USERS,
    payload: users
  };
}

export function setUsers(users: User[] = []): Action<User[]> {
  return {
    type: SET_USERS,
    payload: users
  };
}

export function setFilterLoader(value: boolean, requestId: string = ''): Action<FilterLoader> {
  return {
    type: SET_LOADER,
    payload: { value, requestId }
  };
}

export function activateFilterLoader(reqId: string = ''): ThunkAction<void, State, void> {
  return (dispatch) => {
    dispatch(setFilterLoader(true, reqId));
  };
}

export function disableFilterLoader(reqId: string = ''): ThunkAction<void, State, void> {
  return (dispatch, getState) => {
    const { requestId } = getState().app.filterLoader;

    if (requestId === reqId) {
      dispatch(setFilterLoader(false, ''));
    }
  };
}

export function setUsersLoader(loader: boolean): Action<boolean> {
  return {
    type: SET_USER_LOADER,
    payload: loader
  };
}

export function setNextPage(url: string = ''): Action<string> {
  return {
    type: SET_NEXT_PAGE,
    payload: url
  };
}

export function updateSearch(value: string = ''): Action<string> {
  return {
    type: UPDATE_SEARCH,
    payload: value
  };
}

export function fetchUsers(searchTerm: string = ''): ThunkAction<void, State, void> {
  return async (dispatch) => {
    const url = searchTerm !== '' ? `/api/users?searchTerm=${searchTerm}` : '/api/users';
    const reqId = nanoid();

    dispatch(activateFilterLoader(reqId));

    try {
      const data: ResData = await apiFetch(url);

      dispatch(setUsers(data.result));
      dispatch(setNextPage(data.nextPageUrl));
    } catch (e) {
      console.log(e);
    } finally {
      dispatch(disableFilterLoader(reqId));
    }
  };
}

export function appendUsers(nextPageUrl: string = ''): ThunkAction<void, State, void> {
  return async (dispatch) => {
    dispatch(setUsersLoader(true));

    try {
      const data: ResData = await apiFetch(nextPageUrl);

      dispatch(addUsers(data.result));
      dispatch(setNextPage(data.nextPageUrl));
    } catch (e) {
      console.log(e);   
    } finally {
      dispatch(setUsersLoader(false));
    }
  };
}

/**
 * App reducer
 */
const initialState = Immutable.from<AppState>({
  searchTerm: '',
  filterBy: ['name'],
  nextPageUrl: '',
  filterLoader: {
    value: false,
    requestId: ''
  },
  usersLoader: false,
  users: []
});


export default createReducer<ImmutableState>({
  [INIT_STATE]: state => (
    state.merge({ 
      usersLoader: false,
      filterLoader: { value: false, requestId: '' },
      users: [],
      nextPageUrl: '',
      searchTerm: ''
    })
  ),

  [ADD_USERS]: (state, { payload: users }: Action<User[]>) => (
    state.set('users', state.users.concat(users))
  ),

  [SET_USERS]: (state, { payload: users }: Action<User[]>) => (
    state.set('users', users)
  ),

  [SET_LOADER]: (state, { payload }: Action<FilterLoader>) => (
    state.set('filterLoader', payload)
  ),

  [SET_USER_LOADER]: (state, { payload }: Action<boolean>) => (
    state.set('usersLoader', payload)
  ),

  [SET_NEXT_PAGE]: (state, { payload: url }: Action<string>) => (
    state.set('nextPageUrl', url)
  ),

  [UPDATE_SEARCH]: (state, { payload }: Action<string>) => (
    state.set('searchTerm', payload)
  )
}, initialState);
