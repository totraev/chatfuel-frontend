import React from 'react';
import { connect } from 'react-redux';
import Waypoint from 'react-waypoint';
import debounce from 'lodash.debounce';
import './styles.css';

import { Props, StateProps, DispatchProps, ComponentProps } from './types';
import { State } from '../../store/rootReducer';

import { updateSearch, fetchUsers, appendUsers, setFilterLoader, setUsersLoader } from './duck';
import { filterSelector } from './selectors';

import UserList from '../../components/UserList';
import Input from '../../components/Input';
import Loader from '../../components/Loader';


class App extends React.Component<Props> {
  static defaultProps = {
    debounceTime: 500
  };

  componentDidMount() {
    this.props.fetchUsers(this.props.searchTerm);
  }

  handleFilterUsers = debounce(
    (searchTerm: string) => this.props.fetchUsers(searchTerm),
    this.props.debounceTime
  );
  
  handleChange = (e: React.FormEvent<HTMLInputElement>): void => {
    const { value } = e.currentTarget;

    this.props.setFilterLoader(true);
    this.props.updateSearch(value);
    this.handleFilterUsers(value);
  }

  handleAppendUsers = ({ event }: Waypoint.CallbackArgs): void => {
    const { filterLoader, nextPageUrl } = this.props;

    if (event !== null && !filterLoader && nextPageUrl !== '') {
      this.props.appendUsers(nextPageUrl);
    }
  }

  render() {
    const { searchTerm, filterLoader, usersLoader, users } = this.props;

    return (
      <div styleName="container">
        <Input
          loading={filterLoader}
          value={searchTerm}
          onChange={this.handleChange}/>

        <UserList users={users} loader={filterLoader}/>

        <Waypoint bottomOffset="-2000px" onEnter={this.handleAppendUsers}/>
        <Loader loading={usersLoader}/>
      </div>
    );
  }
}


export default connect<StateProps, DispatchProps, ComponentProps, State>(
  state => ({
    ...state.app,
    users: filterSelector(state),
    filterLoader: state.app.filterLoader.value
  }),
  {
    updateSearch,
    fetchUsers,
    appendUsers,
    setFilterLoader,
    setUsersLoader
  }
)(App);
